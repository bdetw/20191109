import "bootstrap/dist/css/bootstrap.css";
import $ from "jquery";

import Web3 from "web3";
import ecommerceStoreArtifact from "../../build/contracts/EcommerceStore.json";

const App = {
  web3: null,
  account: null,
  ec: null,

  start: async function() {
    const { web3 } = this;

    try {
      // get contract instance
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = ecommerceStoreArtifact.networks[networkId];
      this.ec = new web3.eth.Contract(
        ecommerceStoreArtifact.abi,
        deployedNetwork.address,
      );

      // get accounts
      const accounts = await web3.eth.getAccounts();
      this.account = accounts[0];

      if ($("#index-page").length > 0) {
        this.renderSotre();
      }

      if ($("#list-item-page").length > 0) {
        $("#add-item-to-store").submit(event => {
          event.preventDefault();

          const req = $("#add-item-to-store").serialize();
          let params = JSON.parse('{"' + req.replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
          let decodedParams = {};
          Object.keys(params).forEach( k => decodedParams[k] = decodeURIComponent(decodeURI(params[k])) );

          this.saveProductToBlockchain(decodedParams);
       })   
      }

      //Sprint 5.1
      if ($("#product-page").length > 0) {
        let productId = new URLSearchParams(window.location.search).get('id');
        if (!productId) return $('#msg').html('ERROR: no product id specified.').show();
        this.renderProductDetails(productId);

        //Sprint 5.2
        $("#bidding").submit(event => {
          event.preventDefault();
          this.bidding();
        });
        
        //Sprint 5.3
        $("#revealing").submit(event => {
          event.preventDefault();    
          this.revealing();
        });

      }

    } catch (error) {
      console.error("Could not connect to contract or chain.");
    }
  },

  bidding: async function() {
    $("#msg").hide();
    let amount = $("#bid-amount").val();
    let sendAmount = $("#bid-send-amount").val();
    let secretText = $("#secret-text").val();
    let sealedBid = web3.sha3(web3.toWei(amount, 'ether') + secretText).toString('hex');
    let productId = $("#product-id").val();
    
    const { bid } = this.ec.methods;
    await bid(
      parseInt(productId),
      sealedBid
    ).send({value: web3.toWei(sendAmount), from: this.account, gas: 440000});

    $("#msg").html("Your bid has been successfully submitted!");
    $("#msg").show();

  },

  revealing: async function() {
    $("#msg").hide();
    let amount = $("#actual-amount").val();
    let secretText = $("#reveal-secret-text").val();
    let productId = $("#product-id").val();

    const { revealBid } = this.ec.methods;
    await revealBid(
      parseInt(productId),
      web3.toWei(amount).toString(),
      secretText
    ).send({from: this.account, gas: 440000});

    $("#msg").show();
    $("#msg").html("Your bid has been successfully revealed!");

  },

  //Sprint 5.1
  renderProductDetails: async function(productId) {
    const { getProduct } = this.ec.methods;

    let p = await getProduct(productId).call();

    $("#product-desc").append(`<div>${p[4]}</div>`);
    $("#product-image").append(`<img src='${p[3]}' width='250px'/>`);
    $("#product-price").html(displayPrice(p[7]));
    $("#product-name").html(p[1]);
    $("#product-auction-end").html(displayEndHours(p[6]));
    $("#product-id").val(p[0]);

    //Sprint 5.2
    let currentTime = getCurrentTimeInSeconds();
    $("#revealing, #bidding").hide();
    if(currentTime < p[6]) {
      $("#bidding").show();
    } else if (currentTime  < (p[6] + 600)) {
      $("#revealing").show();
    }

  },
  
  saveProductToBlockchain: async function(params) {

    let auctionStartTime = Date.parse(params["product-auction-start"]) / 1000;
    let auctionEndTime = auctionStartTime + parseInt(params["product-auction-end"]) * 24 * 60 * 60;

    const { addProductToStore } = this.ec.methods;
    await addProductToStore(
      params["product-name"],
      params["product-category"],
      params["product-image"],
      params["product-description"],
      auctionStartTime, auctionEndTime, 
      web3.toWei(params["product-price"], 'ether'), 
      parseInt(params["product-condition"])
    ).send({ from: this.account, gas: 440000 });

    $("#msg").show();
    $("#msg").html("Your product was successfully added to your store!");    

  },

  renderSotre: async function() {
    const { totalProducts } = this.ec.methods;
    const { getProduct } = this.ec.methods;

    const i = await totalProducts().call();
    console.log("totalProducts = " + i);
    let html = "";
    for (let id = 1; id <= i; id++) {
      console.log("id = " + id);
      let p = await getProduct(id).call();
      $("#product-list").append(this.buildProduct(p));
    }

  },

  buildProduct: function(product) {
    const html = `
      <div>
        <a href='/product.html?id=${product[0]}' target='_blank'>
          <img src = '${product[3]}' width='150px' />
        </a>
        <div>${product[1]}</div>
        <div>${product[2]}</div>
        <div>${product[5]}</div>
        <div>${product[6]}</div>
        <div>Eether ${product[7]/web3.toWei('1', 'ether')}</div>
      </div>
    `
    console.log(html);
    return html;

  },

};

window.App = App;

window.addEventListener("load", function() {
  if (window.ethereum) {
    // use MetaMask's provider
    App.web3 = new Web3(window.ethereum);
    window.ethereum.enable(); // get permission to access accounts
  } else {
    console.warn(
      "No web3 detected. Falling back to http://127.0.0.1:8545. You should remove this fallback when you deploy live",
    );
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    App.web3 = new Web3(
      new Web3.providers.HttpProvider("http://127.0.0.1:7545"),
    );
  }

  App.start();
});

function displayPrice(amt) {
  return 'Ξ' + web3.fromWei(amt, 'ether');
}

function getCurrentTimeInSeconds(){
  return Math.round(new Date() / 1000);
}

function displayEndHours(seconds) {
  let current_time = getCurrentTimeInSeconds()
  let remaining_seconds = seconds - current_time;

  if (remaining_seconds <= 0) {
    return "Auction has ended";
  }

  let days = Math.trunc(remaining_seconds / (24*60*60));
  remaining_seconds -= days*24*60*60
  let hours = Math.trunc(remaining_seconds / (60*60));
  remaining_seconds -= hours*60*60
  let minutes = Math.trunc(remaining_seconds / 60);
  if (days > 0) {
    return "Auction ends in " + days + " days, " + hours + ", hours, " + minutes + " minutes";
  } else if (hours > 0) {
    return "Auction ends in " + hours + " hours, " + minutes + " minutes ";
  } else if (minutes > 0) {
    return "Auction ends in " + minutes + " minutes ";
  } else {
    return "Auction ends in " + remaining_seconds + " seconds";
  }
}
