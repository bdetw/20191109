pragma solidity >=0.4.21 <0.6.0;

contract EcommerceStore {
    enum ProductStatus { Open, Sold, Unsold }
    enum ProductCondition { New, Used }

    struct Product {
        //商品基本資訊
        uint id;
        string name;
        string category;
        string imageLink;
        string descLink;
        //拍賣相關資訊
        uint auctionStartTime;
        uint auctionEndTime;
        uint startPrice;
        address highestBidder;
        uint highestBid;
        uint secondHighestBid;
        uint totalBids;
        //商品狀態
        ProductStatus status;
        ProductCondition condition;

    }

    //賣家商品表
    mapping (address => mapping(uint => Product)) stores;
    //商品賣家表
    mapping (uint => address) productIdInStore;

    uint private productIndex;
    constructor() public {
        productIndex = 0;
    }

    //查詢商品總數
    function totalProducts() public view returns (uint) {
        return productIndex;
    }

    function addProductToStore(
        string memory _name,
        string memory _category,
        string memory _imageLink,
        string memory _descLink,
        uint _auctionStartTime,
        uint _auctionEndTime,
        uint _startPrice,
        uint _productCondition
    ) public {
        //檢查拍賣起始時間
        require(_auctionStartTime < _auctionEndTime);

        //商品編號累進
        productIndex += 1;

        //新建產品資訊
        Product memory product = Product(
            productIndex,
            _name,
            _category,
            _imageLink,
            _descLink,
            _auctionStartTime,
            _auctionEndTime,
            _startPrice,
            address(0), 0, 0, 0,
            ProductStatus.Open,
            ProductCondition(_productCondition)
        );

        //存入賣家商品表
        stores[msg.sender][productIndex] = product;

        //存入商品賣家表
        productIdInStore[productIndex] = msg.sender;


    }

    function getProduct(uint _productId) public view  returns (
        uint,
        string memory,
        string memory,
        string memory,
        string memory,
        uint,
        uint,
        uint,
        ProductStatus,
        ProductCondition
    ) {
        Product memory product = stores[productIdInStore[_productId]][_productId];

        return (
            product.id,
            product.name,
            product.category,
            product.imageLink,
            product.descLink,
            product.auctionStartTime,
            product.auctionEndTime,
            product.startPrice,
            product.status,
            product.condition
        );

    }
}