import "bootstrap/dist/css/bootstrap.css";
import $ from "jquery";

import Web3 from "web3";
import ecommerceStoreArtifact from "../../build/contracts/EcommerceStore.json";

const App = {
  web3: null,
  account: null,
  ec: null,

  start: async function() {
    const { web3 } = this;

    try {
      // get contract instance
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = ecommerceStoreArtifact.networks[networkId];
      this.ec = new web3.eth.Contract(
        ecommerceStoreArtifact.abi,
        deployedNetwork.address,
      );

      // get accounts
      const accounts = await web3.eth.getAccounts();
      this.account = accounts[0];

      if ($("#index-page").length > 0) {
        this.renderSotre();
      }

      if ($("#list-item-page").length > 0) {
        $("#add-item-to-store").submit(event => {
          event.preventDefault();
          const req = $("#add-item-to-store").serialize();
          let params = JSON.parse('{"' + req.replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
          let decodedParams = {}
          Object.keys(params)
           .forEach( k => decodedParams[k] = decodeURIComponent(decodeURI(params[k])) )
           this.saveProductToBlockchain(decodedParams);
       })   
      }

    } catch (error) {
      console.error("Could not connect to contract or chain.");
    }
  },

  saveProductToBlockchain: async function(params) {

    let auctionStartTime = Date.parse(params["product-auction-start"]) / 1000;
    let auctionEndTime = auctionStartTime + parseInt(params["product-auction-end"]) * 24 * 60 * 60;

    const { addProductToStore } = this.ec.methods;
    await addProductToStore(
      params["product-name"],
      params["product-category"],
      params["product-image"],
      params["product-description"],
      auctionStartTime, auctionEndTime, 
      web3.toWei(params["product-price"], 'ether'), 
      parseInt(params["product-condition"])
    ).send({ from: this.account, gas: 440000 });

    $("#msg").show();
    $("#msg").html("Your product was successfully added to your store!");    

  },

  renderSotre: async function() {
    const { totalProducts } = this.ec.methods;
    const { getProduct } = this.ec.methods;

    const i = await totalProducts().call();
    console.log("totalProducts = " + i);
    let html = "";
    for (let id = 1; id <= i; id++) {
      console.log("id = " + id);
      let p = await getProduct(id).call();
      $("#product-list").append(this.buildProduct(p));
    }

  },

  buildProduct: function(product) {
    const html = `
      <div>
        <a href='/product.html?id=${product[0]}' target='_blank'>
          <img src = '${product[3]}' width='150px' />
        </a>
        <div>${product[1]}</div>
        <div>${product[2]}</div>
        <div>${product[5]}</div>
        <div>${product[6]}</div>
        <div>Eether ${product[7]/web3.toWei('1', 'ether')}</div>
      </div>
    `
    console.log(html);
    return html;

  },

};

window.App = App;

window.addEventListener("load", function() {
  if (window.ethereum) {
    // use MetaMask's provider
    App.web3 = new Web3(window.ethereum);
    window.ethereum.enable(); // get permission to access accounts
  } else {
    console.warn(
      "No web3 detected. Falling back to http://127.0.0.1:8545. You should remove this fallback when you deploy live",
    );
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    App.web3 = new Web3(
      new Web3.providers.HttpProvider("http://127.0.0.1:7545"),
    );
  }

  App.start();
});
