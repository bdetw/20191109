const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  mode: 'development',
  entry: "./src/index.js",
  output: {
    filename: "index.js",
    path: path.resolve(__dirname, "dist"),
  },
  plugins: [
    new CopyWebpackPlugin([
      { from: "./src/index.html", to: "index.html" },
      { from: "./src/list-item.html", to: "list-item.html" }
    ]),
  ],
  devServer: { contentBase: path.join(__dirname, "dist"), compress: true },
  module: {
    rules: [
        {
            test: /\.css$/, // 針對所有.css 的檔案作預處理，這邊是用 regular express 的格式
            use: [
                'style-loader',  // 這個會後執行 (順序很重要)
                'css-loader' // 這個會先執行
            ]
        }
    ]
  }

};
