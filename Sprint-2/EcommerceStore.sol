pragma solidity >=0.4.21 <0.6.0;

contract EcommerceStore {
    enum ProductStatus { Open, Sold, Unsold }
    enum ProductCondition { New, Used }

    struct Bid {
        address bidder; //競標者
        uint productId; //商品編號
        uint value;     //保證金
        bool revealed;  //已揭示出價
    }

    struct Product {
        //競標出價表
        mapping (address => mapping(bytes32 => Bid)) bids;

        //商品基本資訊
        uint id;
        string name;
        string category;
        string imageLink;
        string desc;
        //競標相關資訊
        uint auctionStartTime;
        uint auctionEndTime;
        uint startPrice;
        address payable highestBidder;
        uint highestBid;
        uint secondHighestBid;
        uint totalBids;
        //商品狀態
        ProductStatus status;
        ProductCondition condition;

    }

    //賣家商品表
    mapping (address => mapping(uint => Product)) stores;
    //商品賣家表
    mapping (uint => address) productIdInStore;

    uint private productIndex;
    constructor() public {
        productIndex = 0;
    }

    //查詢商品總數
    function totalProducts() public view returns (uint) {
        return productIndex;
    }

    function bid(
        uint _productId,    //商品編號
        bytes32 _bid        //競標出價之加密
    ) public payable returns (bool) {
        //透過商品編號查詢商品
        Product storage product = stores[productIdInStore[_productId]][_productId];
        //檢查是否在有效競標期內
        require(now >= product.auctionStartTime);
        require(now <= product.auctionEndTime);
        //檢查保證金是否高於起標價
        require(msg.value > product.startPrice);
        //檢查競標者是否第一次出價
        require(product.bids[msg.sender][_bid].bidder == address(0));
        //存入出價資訊
        product.bids[msg.sender][_bid] = Bid(msg.sender, _productId, msg.value, false);
        //更新競標者人數
        product.totalBids += 1;
        return true;

    }

    function revealBid(
        uint _productId,
        string memory _amount,  //競標時之出價
        string memory _secret   //出價加密之保護碼
    ) public {
        //透過商品編號查詢商品
        Product storage product = stores[productIdInStore[_productId]][_productId];
        //檢查競標已截止
        require(now > product.auctionEndTime);
        //驗證競標出價資訊是否有效
        bytes32 sealedBid = keccak256(abi.encodePacked(_amount, _secret));
        Bid memory bidInfo = product.bids[msg.sender][sealedBid];
        require(bidInfo.bidder != address(0));
        require(bidInfo.revealed == false);

        uint refund;    //退回金額
        uint amount = stringToUint(_amount);

        //若保證金少於出價則該競標無效
        if(bidInfo.value < amount) {
            refund = bidInfo.value;
        } else {
            //是否為第一人
            if (address(product.highestBidder) == address(0)) {
                product.highestBidder = msg.sender;
                product.highestBid = amount;
                product.secondHighestBid = product.startPrice;
                refund = bidInfo.value - amount;
            } else {
                //是否高於目前最高出價
                if (amount > product.highestBid) {
                    product.secondHighestBid = product.highestBid;
                    product.highestBidder.transfer(product.highestBid);
                    product.highestBidder = msg.sender;
                    product.highestBid = amount;
                    refund = bidInfo.value - amount;
                } else if (amount > product.secondHighestBid) {
                    product.secondHighestBid = amount;
                    refund = amount;
                } else {
                    refund = amount;
                }
            }
        }

        //更新競標狀態
        product.bids[msg.sender][sealedBid].revealed = true;

        if (refund > 0) {
            msg.sender.transfer(refund);
        }


    }

    //查詢競標結果
    function highestBidderInfo(uint _productId) public view returns (address, uint, uint) {
        Product memory product = stores[productIdInStore[_productId]][_productId];
        return (product.highestBidder, product.highestBid, product.secondHighestBid);
    }

    //查詢競標者總數
    function totalBids(uint _productId) public view returns (uint) {
        Product memory product = stores[productIdInStore[_productId]][_productId];
        return product.totalBids;
    }

    function stringToUint(string memory s) private pure returns (uint) {
        bytes memory b = bytes(s);
        uint result = 0;
        for (uint i = 0; i < b.length; i++) {
            if (uint8(b[i]) >= 48 && uint8(b[i]) <= 57) {
                result = result * 10 + (uint(uint8(b[i]) - 48));
            }
        }
        return result;
    }

    function addProductToStore(
        string memory _name,
        string memory _category,
        string memory _imageLink,
        string memory _desc,
        uint _auctionStartTime,
        uint _auctionEndTime,
        uint _startPrice,
        uint _productCondition
    ) public {
        //檢查拍賣起始時間
        require(_auctionStartTime < _auctionEndTime);

        //商品編號累進
        productIndex += 1;

        //新建商品資訊
        Product memory product = Product(
            productIndex,
            _name,
            _category,
            _imageLink,
            _desc,
            _auctionStartTime,
            _auctionEndTime,
            _startPrice,
            address(0), 0, 0, 0,
            ProductStatus.Open,
            ProductCondition(_productCondition)
        );

        //存入賣家商品表
        stores[msg.sender][productIndex] = product;

        //存入商品賣家表
        productIdInStore[productIndex] = msg.sender;


    }

    function getProduct(uint _productId) public view  returns (
        uint,
        string memory,
        string memory,
        string memory,
        string memory,
        uint,
        uint,
        uint,
        ProductStatus,
        ProductCondition
    ) {
        Product memory product = stores[productIdInStore[_productId]][_productId];

        return (
            product.id,
            product.name,
            product.category,
            product.imageLink,
            product.desc,
            product.auctionStartTime,
            product.auctionEndTime,
            product.startPrice,
            product.status,
            product.condition
        );

    }
}